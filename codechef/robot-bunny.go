package main

import "fmt"

func main() {
	var tc int
	fmt.Scanf("%d", &tc)
	for i := 0; i < tc; i++ {
		var x, y, x2, y2 int
		fmt.Scanf("%d %d %d %d", &x, &y, &x2, &y2)
		switch {
		case x2 > x && y == y2:
			fmt.Println("right")
		case y2 > y && x2 == x:
			fmt.Println("up")
		case x2 < x && y == y2:
			fmt.Println("left")
		case y2 < y && x2 == x:
			fmt.Println("down")
		default:
			fmt.Println("sad")
		}
	}
}
