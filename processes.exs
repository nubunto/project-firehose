defmodule API do
    def random do
        time = :rand.uniform(1000)
        :timer.sleep(time)
        time
    end
end

parent = self()

spawn_link(fn() ->
    raise("wtf")
end)

receive do
    random -> IO.puts("received #{random}")
after
    500 -> IO.puts("no response received")
end
