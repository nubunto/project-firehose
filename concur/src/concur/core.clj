(ns concur.core
  (:require [clojure.core.async :refer [>!! <!! go >! <! alts!]]))

(def alice (chan))
(def bob (chan))

(defn alice-and-bob-morning-routine
  []
  (start-listening)
  (println "Let's go for a walk!")
  (get-ready)
  (println "alice took " (<!! alice) " getting ready")
  (println "bobA took " (<!! bob) " getting ready"))

(defn get-ready
  []
  (>!! first-channel :get-ready)
  (>!! second-channel :get-ready))

(defn after
  [time]
  (go
    (Thread/sleep time)
    time))

(defn start-listening
  []
  (go (while true
        (let [[v ch] (alts! alice bob)]
          (condp v
              (= :get-ready) (>! ch (<! (after (rand 60000 90000)))))))))
