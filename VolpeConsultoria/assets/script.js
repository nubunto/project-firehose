 jQuery(document).ready(function($) {
    if (window.location.host == 'localhost:8082ws') {
      window.location.href = 'http://localhost:8082/#personal_coaching';
    }

    $(".scroll a, .navbar-brand, .gototop").click(function(event){
    event.preventDefault();
    $('html,body').animate({scrollTop:$(this.hash).offset().top}, 600,'swing');
    $(".scroll li").removeClass('active');
    $(this).parents('li').toggleClass('active');
    });
  });

$('#mail_form').submit(function() {
  var valid_name = check(document.querySelector('#name'));
  if (valid_name !== '') {
    alert(valid_name);
    return false;
  }

  var valid_email = check(document.querySelector('#email'));
  if (valid_email !== '') {
    alert(valid_email);
    return false;
  }

  var valid_message = check(document.querySelector('#message'));
  if (valid_message !== '') {
    alert(valid_message);
    return false;
  }
  return;
});

function check(input) {
  if(input.value === "") {
    input.focus();
    return "O campo ''" + input.name + "'' não pode ser vazio.";
  }

  if(input.validity.typeMismatch){
    input.focus();
    return "O campo '" + input.name + "' não é válido.";
  }

  if(input.validity.tooShort) {4
    input.focus();
    return "Por favor, escreva um pouco mais.";
  }

  if(input.validity.tooLong) {
    input.focus();
    return "O valor está muito longo.";
  }
  return '';
}



var wow = new WOW(
  {
    boxClass:     'wowload',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true        // act on asynchronously loaded content (default is true)
  }
);

wow.init();
$('.carousel').swipe( {
     swipeLeft: function() {
         $(this).carousel('next');
     },
     swipeRight: function() {
         $(this).carousel('prev');
     },
     allowPageScroll: 'vertical'
 });
