# PLANO DE AÇÃO
Criação de site estático institucional.
Conteúdo, imagens e informações relevantes (dados referentes à telefones, e-mails e etc) devem ser pré-definidas.
Páginas a serem desenvolvidas:
 * Página inicial
 * Sobre
 * Serviços
 * Contato

Conteúdo de todas as páginas, quando for necessitado alterações, deve ser solicitado diretamente aos desenvolvedores via meio de comunicação formal.

Temos como padrão, o desenvolvimento de páginas web responsivas. Ou seja, com visualização personalizada e adaptada ao dispositivo em questão (Smartphones, tablets, notebook, etc).

Seguiremos inicialmente, mantendo os mesmos conteúdos presentes no site do wix já existente. (Lembrando que alterações de conteúdo, visuais e de arquitetura de layout estão em aberto, podendo ser solicitado respectivas alterações.)

# ESTIMATIVA

Valor da hora de desenvolvimento: R$30,00
Tempo de entrega: 1 mês (20 horas)
Valor total de desenvolvimento: R$600,00

O valor referente ao domínio não está incluso nas horas de desenvolvimento.
É um valor pago anualmente, e não dá pra incluir em estimativa, devido à enorme variação de valor que existe entre alguns domínios e outros. (variam de $6/ano até $100/ano)
Após aprovação da estimativa, podemos juntos, definir a escolha de um domínio disponível, e que não saia tão caro.
