(ns my-proj.gs)

(+ 1 1)
(defmacro infix [a op b] (op a b))
(infix 1 + 1)
(infix 2 * 3)
