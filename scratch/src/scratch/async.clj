(ns scratch.async
  (:require [clojure.core.async :as async :refer :all]))


(let [c (chan 10)]
  (close! c))

(let [c (chan 10)]
  (>!! c "hello")
  (assert (= "hello" (<!! c)))
  (close! c))

(let [c (chan)]
  (thread (>!! c "hello"))
  (assert (= "hello" (<!! c)))
  (close! c))

(let [c (chan)]
  (go (>! c "hello"))
  (assert (= "hello" (<!! (go (<! c)))))
  (close! c))

(let [c1 (chan)
      c2 (chan)
      r (promise)]
  (thread
    (while true
      (let [[v ch] (alts!! [c1 c2])]
        (deliver r v))))
  (>!! c1 "hi")
  (>!! c2 "there")
  @r)
