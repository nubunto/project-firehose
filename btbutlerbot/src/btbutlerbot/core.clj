(ns btbutlerbot.core
  (:require [clojure.core.async :refer [<!!]]
            [clojure.string :as s]
            [environ.core :refer [env]]
            [morse.handlers :as h]
            [morse.polling :as p]
            [morse.api :as t]
            [btbutlerbot.tokens]
            [clojure.string :as str])
  (:gen-class))

(def token (env :telegram-token))
(defonce users (atom {}))

(h/defhandler handler

  (h/command-fn "start"
    (fn [{{id :id :as chat} :chat}]
      (t/send-text token id "Welcome to butlerbot! How can I assist you today?")))

  (h/command-fn "help"
    (fn [{{id :id :as chat} :chat}]
      (t/send-text token id "You can tell me these commands:\n\n/credentials <username> <password> - stores your credentials\n/token <one-time-token> - I will try my best to give you a valid token")))

  (h/command-fn "token"
    (fn [{{id :id :as chat} :chat, text :text}]
      (let [user (get @users id)
            [_ one-time-token] (s/split text #" ")]
        (if (nil? user)
          (t/send-text token id "Update your information before using this bot with the /credentials command")
          (let [{result :result, ok :ok} (btbutlerbot.tokens/new-token user one-time-token)]
            (if ok
              (t/send-text token id (str "Here is your token: " result))
              (t/send-text token id "I wasn't able to retrieve your token. Sorry!")))))))

  (h/command-fn "credentials"
    (fn [{{id :id :as chat} :chat, text :text}]
      (when-let [[_ username password one-time-token] (s/split text #" ")]
        (swap! users assoc id {:username username, :password password})
        (t/send-text token id "Configured username and password successfully"))))

  (h/message-fn
    (fn [{{id :id} :chat, text :text}]
      (t/send-text token id "I'm sorry, I don't understand that. Could you please see your options with /help?"))))

(defn -main
  [& args]
  (when (s/blank? token)
    (println "Please provde token in TELEGRAM_TOKEN environment variable!")
    (System/exit 1))

  (println "Starting the btbutlerbot")
  (<!! (p/start token handler)))
