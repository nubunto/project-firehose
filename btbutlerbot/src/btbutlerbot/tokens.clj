(ns btbutlerbot.tokens
  (:require [clojure.java.shell :refer [sh]]
            [environ.core :refer [env]]))

(def bt-token-sh (env :bt-token-sh))

; TODO: vendor the bt-token code
(defn new-token
  [credentials one-time-token]
  (let [result (sh
                 bt-token-sh "--silent"
                 :env {
                       "BT_USERNAME" (:username credentials)
                       "BT_PASSWORD" (:password credentials)}
                 :in one-time-token)]
    {:ok (= (:exit result) 0)
     :result (:out result)}))
