import './registerServiceWorker';
import { connect } from 'inferno-mobx';

import Users from '../Users/Users';
import AddUser from '../Users/AddUser';

function App(props) {
    return (
        <div className="App">
            <Users {...props} />
            <AddUser {...props} />
        </div>
    );
}


export default connect(['userStore'], App);
