import { render } from 'inferno';
import App from './App';
import { UserStore } from '../../stores';

it('renders without crashing', () => {
    const div = document.createElement('div');
    render(<App userStore={UserStore()}/>, div);
});

