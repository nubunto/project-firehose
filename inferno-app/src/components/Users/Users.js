import { connect } from 'inferno-mobx';
import User from './User';

function Users({userStore}) {
    if (!userStore.hasUsers) {
        return (
            <div>
                No users!
            </div>
        );
    }
    return (
        <ul>
            {userStore.list.map(user => <User {...user} />)}
        </ul>
    )
}

export default connect(['userStore'], Users);
