import { render } from 'inferno';
import AddUser from './AddUser';
import { users } from '../stores';

describe('Add User component', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        render(<AddUser userStore={users}/>, div);
    });
});
