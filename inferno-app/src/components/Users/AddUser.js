import { linkEvent } from 'inferno';
import { connect } from 'inferno-mobx';
import { observable, action } from 'mobx';

const state = observable({
    name: '',
    setName: action(function(name) {
        this.name = name;
    })
});

function addUser({userStore}) {
    userStore.addUser({name: state.name});
    state.setName('');
}

function AddUser(props) {
    return (
        <div>
            <label>Name: </label>
            <input type="text" onChange={(e) => state.setName(e.target.value)} value={state.name} />
            <button onClick={linkEvent(props, addUser)}>Add User</button>
        </div>
    );
}

export default connect(['userStore'], AddUser);
