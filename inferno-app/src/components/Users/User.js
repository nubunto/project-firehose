import { connect } from 'inferno-mobx';

function User({name}) {
    return (
        <li>
            {name}
        </li>
    );
}

export default connect(['userStore'], User);
