import { render } from 'inferno';
import App from './components/App/App';
import { Provider } from 'inferno-mobx';
import { useStrict } from 'mobx';
import { UserStore } from './stores';

useStrict(true);

render(<Provider userStore={UserStore()}><App /></Provider>, document.getElementById('app'));
