import { observable, action, computed } from 'mobx';

const UserStore = () => {
    return observable({
        list: [],

        hasUsers: computed(function() {
            return this.list.length > 0;
        }),

        addUser: action(function(user) {
            this.list.push(user);
        })
    });
}

export { UserStore };
