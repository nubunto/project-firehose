import { UserStore } from '../stores';

describe('User store', () => {
    test('smoke test', () => {
        const store = UserStore();
        expect(store).toBeTruthy();
        expect(UserStore).toBeTruthy();
    });
    test('creates a user successfully', () => {
        const store = UserStore();
        store.addUser({name: 'bar'});
        expect(store.list).toContainEqual({name: 'bar'});
    });
});
