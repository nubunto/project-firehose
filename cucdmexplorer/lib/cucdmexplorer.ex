defmodule CUCDM do
  def cucdm_url(resource), do: "https://172.25.164.252/api/relation/" <> resource
  def credentials, do: {"sm@bt.com", "Newuser@123"}

  def resource(r) do
    %HTTPotion.Response{body: body} = HTTPotion.get!(cucdm_url(r), [basic_auth: credentials(), follow_redirects: true])
    Poison.decode(body)
  end
end
