(ns ^:figwheel-no-load cljs-frontend.dev
  (:require [cljs-frontend.core :as core]))

(enable-console-print!)

(core/init!)
