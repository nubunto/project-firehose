(ns cljs-frontend.prod
  (:require [cljs-frontend.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
