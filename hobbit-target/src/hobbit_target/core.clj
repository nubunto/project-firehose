(ns hobbit-target.core
  (:gen-class)
  (:require [clojure.string :as s]
            [clojure.pprint :refer [pprint]]))

(def hobbit-parts [{:name "head" :size 3}
                   {:name "left-eye" :size 1}
                   {:name "chest" :size 10}
                   {:name "left-arm" :size 2}
                   {:name "left-leg" :size 2}
                   {:name "left-foot" :size 1}])

(defn- symmetrizable?
  [part]
  (re-find #"left-" (:name part)))

(defn- matching-part
  [part]
    {:name (s/replace (:name part) #"left-" "right-")
     :size (:size part)})

(defn- symmetrize-body-parts
  [body-parts part]
  (if (symmetrizable? part)
    (conj body-parts (matching-part part) part)
    (conj body-parts part)))

(defn- sym-body-parts
  [body-parts]
  (reduce symmetrize-body-parts [] body-parts))

(defn- hit
  [asym-body-parts]
  (let [sym-parts (sym-body-parts asym-body-parts)
        body-part-size-sum (reduce + (map :size sym-parts))
        target (rand body-part-size-sum)]
    (loop [[part & remaining] sym-parts
           accumulated-size (:size part)]
      (if (> accumulated-size target)
        part
        (recur remaining (+ accumulated-size (:size (first remaining))))))))

(defn -main
  [& args]
  (do
    (dotimes [i 20]
      (println (str "hit #" (+ i 1)))
      (pprint (hit hobbit-parts)))))
