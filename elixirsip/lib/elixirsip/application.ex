defmodule Elixirsip.Application do
  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      # Starts a worker by calling: Elixirsip.Worker.start_link(arg1, arg2, arg3)
      worker(Elixirsip.Worker, [nil, [name: Elixirsip.Worker]]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Elixirsip.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

defmodule Elixirsip.Worker do
  use GenServer

  def start_link(state, opts \\ []) do
    GenServer.start_link(__MODULE__, state, opts) 
  end

  def handle_call(:hi, _from, state) do
    {:reply, "hello!", state}
  end
end
