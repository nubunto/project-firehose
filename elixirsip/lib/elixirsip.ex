defmodule Elixirsip do
  @moduledoc """
  Documentation for Elixirsip.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Elixirsip.hello
      :world

  """
  def hello do
    :world
  end
end
