(ns markov-elear.generator)

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(def example "And the Golden Grouse And the Pobble who")
(def words (clojure.string/split example #" "))
(def word-transitions (partition-all 3 1 words))

(reduce (fn [r t]
          (merge-with clojure.set/union r
            (let [[a b c] t]
              {[a b] (if c #{c} #{})})))
  {}
  word-transitions)
                   
(merge-with clojure.set/union {:a #{1}} {:a #{2}})
