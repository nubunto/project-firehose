import xs from 'xstream';
import {hr, h1, ul, li, div, label, input} from '@cycle/dom';

const fixerRequest$ = (base) => xs.of({
    url: `https://api.fixer.io/latest?base=${encodeURIComponent(base)}`,
    category: 'fixer'
})

function intent(sources) {
    const request$ = sources.DOM.select('.base')
        .events('change')
        .map(ev => ev.target.value)
        .startWith('USD')
        .filter(base => base.length >= 3)
        .map(base => fixerRequest(base))

    const fixerResponse$ = sources.HTTP.select('fixer').flatten()

    return {
        request$,
        fixerResponse$,
    };
}

function model({request$, fixerResponse$}) {
    return xs.combine(base$, fixerResponse$)
        .map(([base, fixerResponse]) => ({
            base,
            response: fixerResponse.body
        }))
}

function renderRates(rates) {
    const rateCodes = Object.keys(rates);
    return ul(rateCodes.map(rateCode => li(`${rateCode} - ${rates[rateCode]}`)))
}

function view(state$) {
    const vdom$ = state$.map(fixer => {
        if (fixer === null) {
            return div('loading...');
        }
        return div([
            h1(fixer.response.base),
            label('New base:'), input('.base', {type: 'text'}),
            hr(),
            renderRates(fixer.response.rates)
        ]);
    });
    return vdom$;
}

export function App (sources) {
    const vdom$ = view(model(intent(sources)));
    const request$ = fixerRequest$;
    return {
        DOM: vdom$,
        HTTP: request$
    };
}
